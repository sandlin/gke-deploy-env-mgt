### Cluster management project

[Docs](https://docs.gitlab.com/ee/user/clusters/management_project_template.html)

[Applications installed in the cluster](https://docs.gitlab.com/ee/user/infrastructure/clusters/manage/management_project_applications)

[Documentation for this template](https://docs.gitlab.com/ee/user/clusters/management_project_template.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/cluster-management).
